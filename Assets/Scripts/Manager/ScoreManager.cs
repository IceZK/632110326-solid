using System;

public class ScoreManager : Singleton<ScoreManager>
{
    private int score = 0;
    public Action<int> OnScoreChanged;

    public void AddScore(int _points)
    {
        score += _points;
        OnScoreChanged?.Invoke(score);
    }

}
